﻿                                                /** MODULO I - UNIDAD II - CONSULTAS DE SELECCION 1 **/

USE ciclistas;

/* 1.1 Listar las edades de los ciclistas (sin repetidos) */
SELECT DISTINCT
  edad  -- proyección 
FROM
  ciclista  -- relación (tabla que utiliza)
ORDER BY
  edad
;

/* 1.2 Listar las edades de los ciclistas de Artiach */
SELECT 
  DISTINCT edad -- proyección
FROM
  ciclista  -- relación
WHERE
  nomequipo='ARTIACH' -- selección 
ORDER BY
  edad
;


/* 1.3 Listar las edades de los ciclistas de Artiach o de Amore Vita */
SELECT 
  DISTINCT edad 
FROM
  ciclista
WHERE
  nomequipo LIKE 'ARTIACH' OR nomequipo LIKE 'AMORE VITA'
ORDER BY
  edad
;

  -- con el operador extendido IN
  SELECT 
    DISTINCT edad 
  FROM
    ciclista
  WHERE
    nomequipo IN ('ARTIACH','AMORE VITA')
  ORDER BY
    edad
  ;

  -- con el operador UNION
    (
      SELECT 
        edad
      FROM
        ciclista
      WHERE
        nomequipo='ARTIACH'
    )
  UNION
    (
      SELECT 
        edad
      FROM
        ciclista
      WHERE
        nomequipo='AMORE VITA'
    )
  ;


/* 1.4 Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30 */
SELECT 
  dorsal
FROM
  ciclista
WHERE
  edad < 25
  OR edad > 30
;

-- con BETWEEN
SELECT 
  dorsal
FROM
  ciclista
WHERE
  edad NOT BETWEEN 25 AND 30
;

/* 1.5 Listar los dorsales de los ciclistas cuya edad este entre los 28 y 32 y ademas que solo sean de Banesto */
SELECT 
  dorsal 
FROM 
  ciclista
WHERE
  edad BETWEEN 28 AND 32
  AND nomequipo LIKE 'BANESTO'
;

/* 1.6 Indicame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8 */
SELECT
  nombre 
FROM
  ciclista
WHERE
  CHAR_LENGTH(nombre) > 8
;

/* 1.7 Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo
       denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas */
SELECT c.nombre,
       c.dorsal,
       UPPER(c.nombre) nombre_mayusculas
FROM ciclista c
;

/* 1.8 Listar todos los ciclistas que han llevado el maillot MGE (amarillo) en alguna etapa */
SELECT DISTINCT
  l.dorsal
FROM
  lleva l
WHERE
  l.código = 'MGE'
;

/* 1.9 Listar el nombre de los puertos cuya altura sea mayor que 1500 */
SELECT
  p.nompuerto
FROM
  puerto p
WHERE
  p.altura > 1500
;

/* Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea
   mayor que 8 o cuya altura este entre 1800 y 3000 */
SELECT
  p.dorsal
FROM
  puerto p
WHERE
  p.pendiente > 8
    OR p.altura BETWEEN 1800 AND 3000
;

/* 1.11 Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea
   mayor que 8 y cuya altura este entre 1800 y 3000 */
SELECT
  p.dorsal
FROM
  puerto p
WHERE
  p.pendiente > 8
    AND p.altura BETWEEN 1800 AND 3000
;